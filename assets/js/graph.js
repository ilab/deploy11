// Radar chart data

let sphereData;

// State of the data after filters.
let sphereDataState;

let lastCompanySelected;
let lastSphereSelected;





const queryString = window.location;
const sphereDataInfo = queryString.search.split('&');
let sphereName = sphereDataInfo[0].substring(1).split('=')[1] 
sphereName = settleSphere(sphereName);
let companyName = sphereDataInfo[1].split('=')[1] 
const company_file = 'assets/js/companies.json'; 
const data_filename = 'assets/js/sphere-watch-data-new.json';
let selectedSphere = document.querySelector('.js-selected-sphere');

//selectedSphere.innerHTML += setSphereHeading(sphereName).charAt(0).toUpperCase() + setSphereHeading(sphereName).slice(1);


// let graphData = generateRadarGraphData(companyName, sphereData)
// console.debug(graphData);

// Populating options in index page.
let optionElement = document.querySelector('.js-company-options');

// Populate options
fetchJSONFile(company_file)
    .then(data => {
        data.forEach(function(company) {
            var option = document.createElement('option'); 
            option.value = company.name;
            option.text = company.name; // Assuming value to be lowercase, you can change it according to your requirement
            optionElement.add(option);
         });
        
        if (companyName != 'all'){
               optionElement.value = companyName;
         
        } else {
            optionElement.value = '';
        }
      lastCompanySelected = companyName;

    })
    .catch(error => {
        console.error(error);
    });
//

// Change for option
optionElement.addEventListener('change', async function() {
    let filteredData;
     lastCompanySelected = optionElement.value;
     
     
     let sphereData = await fetchJSONFile(data_filename)
     sphereData = sortedCardsInNewest(sphereData)
     removeDivsByClassName('low-results') 
     removeCards();
     if (lastCompanySelected !== '') { 
        filteredData = sphereData.filter(data => data.company === lastCompanySelected && data.sphere === lastSphereSelected);
    } else {
        
        filteredData = sphereData.filter(data => data.sphere === lastSphereSelected);
        
    }
     console.debug("Getting data for company change!!")
     
     filteredData.forEach(data => {
                
                let cardURL = document.createElement('a');
                
                cardURL.href = data.url;
                cardURL.className +='c-card__title';
                let cardDiv = document.createElement('div');
                cardDiv.innerHTML +=data.name;
                cardDiv.appendChild(cardURL);

                let cardList =document.getElementsByClassName('c-card-list');
                cardDiv.className='c-card';
                
                cardList[0].appendChild(cardDiv)
            }) 
           // await drawRadarChart();
        }); 


let selectElement = document.getElementsByClassName('js-sphere-options');
selectElement[0].value=sphereName
lastSphereSelected = sphereName;

selectElement[0].addEventListener('change', async function() {
lastSphereSelected = selectElement[0].value;
//selectedSphere.innerHTML = "Area within " + setSphereHeading(lastSphereSelected).charAt(0).toUpperCase() + setSphereHeading(lastSphereSelected).slice(1);
    let filteredData;
    let sphereData = await fetchJSONFile(data_filename)
    
    sphereData = sortedCardsInNewest(sphereData)
    removeDivsByClassName('low-results') 
    removeCards();
    if (lastCompanySelected !== 'all' ) { 
        filteredData = sphereData.filter(data => data.company === lastCompanySelected && data.sphere === lastSphereSelected);
    } else {
        filteredData = sphereData.filter(data =>  data.sphere === lastSphereSelected);
    }
    console.debug("Getting data for sphere change!")
     filteredData.forEach(data => {
        
                let cardURL = document.createElement('a');
                
                cardURL.href = data.url;
                cardURL.className +='c-card__title';
                let cardDiv = document.createElement('div');
                cardDiv.innerHTML +=data.name;
                cardDiv.appendChild(cardURL);

                let cardList =document.getElementsByClassName('c-card-list');
                cardDiv.className='c-card';
                
                cardList[0].appendChild(cardDiv)
            }) 
            //await drawRadarChart();
});



// Get the data from the file
async function fetchJSONFile(filename) {
    try {
        const response = await fetch(filename);
        if (!response.ok) {
            throw new Error('Failed to fetch file');
        }
        return await response.json();
    } catch (error) {
        console.error(error);
    }
}



window.onload = async function() {
    // Your method or function to run when the page loads
    // Call your method here
    sphereData = await fetchJSONFile(data_filename); 
    sphereData = sortedCardsInNewest(sphereData);

    fetchCardData(sphereData);
    let graphDiv = document.querySelector('.no-graph');
    let messageDiv = document.createElement('div');
    messageDiv.innerHTML +="Please select a company or a sphere to view the plot!";
    messageDiv.className = 'no-info';
    //graphDiv.appendChild(messageDiv);

    
    
};


function removeDivsByClassName(className) {
    var elements = document.getElementsByClassName(className);
    // Convert HTMLCollection to Array and iterate through each element to remove them
    Array.from(elements).forEach(function(element) {
        element.remove();
    });
}




function settleSphere(sphereName) {
    console.debug("Finding sphere >>",sphereName)
    if (sphereName.includes('%20'))  sphereName = sphereName.replaceAll("%20", "")
    console.debug("Preprocess Heading>>",sphereName)
    if(sphereName ==='politics') return 'tourism';
    if(sphereName ==='publicadministration') return 'government';
    if(sphereName ==='communicationandintimacy') return 'communication';
    if(sphereName ==='safetyandsecurity') return 'safety';
    if(sphereName ==='newsandmedia') return 'news';
    if(sphereName ==='outerspace') return 'space';
    if(sphereName ==='scienceandanalytics') return 'science';
    if(sphereName ==='artandculture') return 'arts';
    if(sphereName ==='healthandmedicine') return 'health';
    if(sphereName ==='financeanddigitalcurrencies') return 'finance';
    if(sphereName ==='foodandagriculture') return 'food'; //Urban space
    if(sphereName ==='urbanspace') return 'urban'; 
    if(sphereName ==='leisure,gamesandtoys') return 'leisure';
    if(sphereName ==='mobilityandtransportation') return 'transportation';

    return sphereName;
    

}

function setSphereHeading(sphereName){
    switch(sphereName){
        case 'tourism': return 'Politics'
        case 'government': return 'Public'
        default: return sphereName
    }
}


async function drawRadarChart() {
    let graphData = await generateRadarGraphData();
    
    let noInfo = document.querySelector('.no-info');
    if (noInfo) {
        noInfo.remove();
    }
    
    if (Object.keys(graphData).length < 2) {
        
        let graphDiv = document.querySelector('.no-graph');
        let messageDiv = document.createElement('div');
        messageDiv.innerHTML +="Not enough data! Please select a company or different sphere";
        messageDiv.className = 'no-info';
        graphDiv.appendChild(messageDiv);
        
    } 
    else{
    const data = {
        labels: Object.keys(graphData),
        datasets: [{
            label: '',
            data: Object.values(graphData),
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            borderColor: '#023047',
            borderWidth: 2,
        }]
    };

    const canvas = document.getElementById('radar-chart');
    const ctx = canvas.getContext('2d');

    const centerX = canvas.width / 2;
    const centerY = canvas.height / 2;
    const radius = Math.min(centerX, centerY) * 0.6; // Adjust the multiplier as needed

    const numValues = data.labels.length;
    const angle = Math.PI * 2 / numValues;

    // Clear canvas
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // Draw white background
    ctx.fillStyle = '#fff';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    // Draw grid lines
    ctx.strokeStyle = '#ddd'; // Grid line color
    for (let i = 0; i < numValues; i++) {
        const x = centerX + radius * Math.cos(angle * i);
        const y = centerY + radius * Math.sin(angle * i);
        ctx.beginPath();
        ctx.moveTo(centerX, centerY);
        ctx.lineTo(x, y);
        ctx.stroke();
    }

    // Draw radar lines
    ctx.strokeStyle = '#000';
    ctx.beginPath();
    for (let i = 0; i < numValues; i++) {
        const x = centerX + radius * Math.cos(angle * i);
        const y = centerY + radius * Math.sin(angle * i);
        ctx.lineTo(x, y);
    }
    ctx.closePath();
    ctx.stroke();

    // Fill radar area
    //ctx.fillStyle = data.datasets[0].backgroundColor;

    

    // Draw data points and radar joining lines
    ctx.fillStyle = data.datasets[0].borderColor;
    ctx.strokeStyle = data.datasets[0].borderColor;
    ctx.lineWidth = data.datasets[0].borderWidth;
    ctx.beginPath();
    for (let i = 0; i < numValues; i++) {
        const value = data.datasets[0].data[i];
        const x = centerX + (radius * value / 10) * Math.cos(angle * i);
        const y = centerY + (radius * value / 10) * Math.sin(angle * i);
        if (i === 0) {
            ctx.moveTo(x, y);
        } else {
            ctx.lineTo(x, y);
        }
        // Draw connecting lines from center to data points
        ctx.lineTo(centerX, centerY);
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(x, y, 4, 0, Math.PI * 2);
        ctx.fill();
    }

    // Draw labels
    ctx.fillStyle = '#333';
    ctx.font = '12px Arial';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    for (let i = 0; i < numValues; i++) {
        const x = centerX + (radius + 20) * Math.cos(angle * i);
        const y = centerY + (radius + 20) * Math.sin(angle * i);
        ctx.fillText(data.labels[i], x, y);
    }

    // Draw legend
    ctx.fillStyle = data.datasets[0].backgroundColor;
}
}

//filterSphereForSelection();
filterSphereForLevels();

  
// Filter related data

function filterSphereCompanyData(data, sphereType, companyName){
    if (companyName !== 'all'){
        return data.filter( sphere  => {
            if (sphere.sphere === sphereType && sphere.company === companyName){
                return sphere
            }
        })
    }
    else {
        return data.filter(sphere  => {
            if(sphere.sphere === sphereType) {
                
                return sphere
            } 
            
            
        });
    }
    
}

// async function filterSphereForSelection(){
//     let selectElement = document.getElementById('subspheres');
//     let data = await fetchJSONFile(data_filename)
//     let OnlySubspheres = data.filter(sphere => sphere.subspheres).map(sphere => sphere.subspheres)
//     OnlySubspheres  = [...new Set(OnlySubspheres)];
//     OnlySubspheres.forEach(subspheres => {
//         var option = document.createElement('option');
//         option.text = subspheres;
//         selectElement.add(option)

//     })

// }

async function filterSphereForLevels(){
    let selectElement = document.getElementById('level');
    let data = await fetchJSONFile(data_filename)
    let OnlySubspheres = data.filter(sphere => sphere.subspheres).map(sphere => sphere.level)
    OnlySubspheres = OnlySubspheres.map(item => {
    if (typeof item === 'string') {
        
        return parseInt(item); // 
    }
    return item;
    });
    OnlySubspheres  = [...new Set(OnlySubspheres)];
    OnlySubspheres.forEach(subspheres => {
        var option = document.createElement('option');
        option.text = subspheres;
        selectElement.add(option)

    })

}

function fetchCardData(sphereData){
      
      if (sphereData.length !==0) {

        // Remove any default message
        let cardLoader =document.getElementsByClassName('c-card-list-loader');
        cardLoader[0].style.display='none'

        //Get only card data
        let cardData;
        let sphereName = sphereDataInfo[0].substring(1).split('=')[1]
        sphereName = settleSphere(sphereName); 
        console.info('Sphere Name', sphereName);
        let companyName = sphereDataInfo[1].split('=')[1] 
        let filteredSphereData = filterSphereCompanyData(sphereData,sphereName, companyName)
        if (filteredSphereData.length !==0 ){

            //Remove no event heading
            

            cardData = filteredSphereData.map(sphere => sphere);
        
            // Remove any undefines. TODO: Check the issue in details.
            cardData = cardData.filter(data => data!=undefined);

            // Add filters here
            // Level 
            // Instance 
            cardData.forEach(data => {
               

                let cardURL = document.createElement('a');
                
                cardURL.href = data.url;
                cardURL.className +='c-card__title';
                let cardDiv = document.createElement('div');
                cardDiv.innerHTML +=data.name;
                cardDiv.appendChild(cardURL);

                let cardList =document.getElementsByClassName('c-card-list');
                cardDiv.className='c-card';
                
                cardList[0].appendChild(cardDiv)
            }) 
        } else {
            console.debug("No data!!")
            let noResult = document.createElement('div');
            noResult.innerHTML +='No event Found!';
            let cardList =document.getElementsByClassName('c-card-list');
            noResult.className='low-results';
            cardList[0].appendChild(noResult)

        }
      
    }   
}

function removeCards(){
   let cardElements= document.getElementsByClassName('c-card');
      if(cardElements.length !== 0) {
          for (element in cardElements){
                while(cardElements[element] && cardElements[element].nextSibling){
                    cardElements[element].remove();
                }
                if (cardElements[0]){
                    cardElements[0].remove();
                }
      }
    
   }
   
 
}

function fetchCardDataByInstance(sphereData, instanceFilter){
      if (sphereData.length !==0) {

        // Remove any default message
        let cardLoader =document.getElementsByClassName('c-card-list-loader');
        cardLoader[0].style.display='none'

        //Get only card data
        let cardData;

        let filteredSphereData = sphereData.filter(sphere => sphere.subspheres == instanceFilter);
        if (filteredSphereData.length !==0 ){
            cardData = filteredSphereData.map(sphere => sphere);
        
            // Remove any undefines. TODO: Check the issue in details.
            cardData = cardData.filter(data => data!=undefined);

            // Add filters here
            // Level 
            // Instance 
            cardData.forEach(data => {
                let cardURL = document.createElement('a');
                cardURL.href = data.url;
                cardURL.className +='c-card__title';
                let cardDiv = document.createElement('div');
                
                cardDiv.innerHTML +=data.name;
                cardDiv.appendChild(cardURL);
                let cardList =document.getElementsByClassName('c-card-list');
                cardDiv.className='c-card';
                cardList[0].appendChild(cardDiv)
            }) 
        } else {
            
            let noResult = document.createElement('div');
            noResult.innerHTML +='No event Found!';
            let cardList =document.getElementsByClassName('c-card-list');
            noResult.className='low-results';
            cardList[0].appendChild(noResult)

        }
      
    }   
}

function fetchCardDataByLevel(sphereData, levelFilter){
      if (sphereData.length !==0) {

        // Remove any default message
        let cardLoader =document.getElementsByClassName('c-card-list-loader');
        cardLoader[0].style.display='none'

        //Get only card data
        let cardData;

        let filteredSphereData = sphereData.filter(sphere => sphere.level == levelFilter);
        if (filteredSphereData.length !==0 ){
            cardData = filteredSphereData.map(sphere => sphere);
        
            // Remove any undefines. TODO: Check the issue in details.
            cardData = cardData.filter(data => data!=undefined);

            // Add filters here
            // Level 
            // Instance 
            cardData.forEach(data => {
                let cardURL = document.createElement('a');
                cardURL.href = data.url;
                cardURL.className +='c-card__title';
                let cardDiv = document.createElement('div');
                
                cardDiv.innerHTML +=data.name;
                cardDiv.appendChild(cardURL);
                let cardList =document.getElementsByClassName('c-card-list');
                cardDiv.className='c-card';
                cardList[0].appendChild(cardDiv)
            }) 
        } else {
            
            let noResult = document.createElement('div');
            noResult.innerHTML +='No event Found!';
            let cardList =document.getElementsByClassName('c-card-list');
            noResult.className='low-results';
            cardList[0].appendChild(noResult)

        }
      
    }   
}

function fetchCardDataSorted(sphereData, sort){
       if (sphereData.length !==0) {

        // Remove any default message
        let cardLoader =document.getElementsByClassName('c-card-list-loader');
        cardLoader[0].style.display='none'

        //Get only card data
        let cardData;

        let filteredSphereData = sphereData.filter(sphere => sphere.level == levelFilter);
        if (filteredSphereData.length !==0 ){
            cardData = filteredSphereData.map(sphere => sphere);
        
            // Remove any undefines. TODO: Check the issue in details.
            cardData = cardData.filter(data => data!=undefined);

            // Add filters here
            // Level 
            // Instance 
            cardData.forEach(data => {
                let cardURL = document.createElement('a');
                cardURL.href = data.url;
                cardURL.className +='c-card__title';
                let cardDiv = document.createElement('div');
                cardDiv.innerHTML +=data;
                cardDiv.appendChild(cardURL);
                let cardList =document.getElementsByClassName('c-card-list');
                cardDiv.className='c-card';
                cardList[0].appendChild(cardDiv)
            }) 
        } else {
            
            let noResult = document.createElement('div');
            noResult.innerHTML +='No event Found!';
            let cardList =document.getElementsByClassName('c-card-list');
            noResult.className='low-results';
            cardList[0].appendChild(noResult)

        }
      
    }   
}


// Fill selections 

////////////////////////////////////////////////////////////////
//////////////// Change section ////////////////////////////////
////////////////////////////////////////////////////////////////
let levelSelectElement = document.getElementById('level');
levelSelectElement.addEventListener('change', async function() {
    // Get the selected option
    var selectedOption = levelSelectElement.value;
    sphereData = await fetchJSONFile(data_filename); 
    sphereData = sortedCardsInNewest(sphereData)
    if (selectedOption !='all'){
         removeCards();
        fetchCardDataByLevel(sphereData, selectedOption);
    } else {
        removeCards();
        fetchCardData(sphereData)
        removeChartElement();
    }
})

// let instanceSelectElement = document.getElementById('subspheres');
// instanceSelectElement.addEventListener('change', async function() {
//     // Get the selected option
//     var selectedOption = instanceSelectElement.value;
//     sphereData = await fetchJSONFile(data_filename); 
//     sphereData = sortedCardsInNewest(sphereData)
//     if (selectedOption !='all'){
//          removeCards();
//         fetchCardDataByInstance(sphereData, selectedOption);
//     } else {
//         removeCards();
//         fetchCardData(sphereData)
//     }
       

// })

 document.getElementById('date_asc').addEventListener('change', async function() {
        // Check if the checkbox is checked
        if (this.checked) {
            console.debug("Checkbox clicked!")
            sphereData = await fetchJSONFile(data_filename); 
            removeCards();
            sphereData.sort((a, b) => new Date(a.date) - new Date(b.date));
            sphereData.forEach(data => {
                let cardURL = document.createElement('a');
                cardURL.href = data.url;
                cardURL.className +='c-card__title';
                let cardDiv = document.createElement('div');
                cardDiv.innerHTML +=data.name;
                cardDiv.appendChild(cardURL);
                let cardList =document.getElementsByClassName('c-card-list');
                cardDiv.className='c-card';
                cardList[0].appendChild(cardDiv)
            }) 


        } else {
            sphereData = await fetchJSONFile(data_filename); 
            removeCards();
            sphereData.sort((a, b) => new Date(b.date) - new Date(a.date));
            sphereData.forEach(data => {
                let cardURL = document.createElement('a');
                cardURL.href = data.url;
                cardURL.className +='c-card__title';
                let cardDiv = document.createElement('div');
                cardDiv.innerHTML +=data.name;
                cardDiv.appendChild(cardURL);
                let cardList =document.getElementsByClassName('c-card-list');
                cardDiv.className='c-card';
                cardList[0].appendChild(cardDiv)
            }) 
        }
  
    });

// Function to split and count words
function countWords(text) {
  return text.split("; ").reduce((acc, word) => {
    acc[word] = (acc[word] || 0) + 1;
    return acc;
  }, {});
}

// Generate Radar graph data
async function generateRadarGraphData(){
    let sphereData = await fetchJSONFile(data_filename)
    // Only get company data
    let companySphere = sphereData.filter(data => data.company === lastCompanySelected);
    
    let companyWithAllSpheres = companySphere.filter( data => data.sphere === lastSphereSelected);
    
    let subSpheresInCompanySphere = companyWithAllSpheres.map(data => data.subspheres);
    let countSubpsheres = subSpheresInCompanySphere.reduce((acc, item) => {
  const itemFrequencies = countWords(item);
  for (const word in itemFrequencies) {
    acc[word] = (acc[word] || 0) + itemFrequencies[word];
  }
  return acc;
}, {});
    return topSubSpheres(countSubpsheres)

}

function topSubSpheres(data){
    // Sort the object entries by value in descending order
    const sortedEntries = Object.entries(data).sort((a, b) => b[1] - a[1]);

    // Get the top 12 entries or all entries if there are fewer than 12
    items =   sortedEntries.slice(0, Math.min(12, sortedEntries.length));


    // 4. (Optional) Convert back to object (if needed)
    return Object.fromEntries(items);


}

function sortedCardsInNewest(sphereData){
    let sortedData = sphereData.sort((a, b) => new Date(b.date) - new Date(a.date));
    return sortedData
}


