
let isDragging;
let linkTimer;
let linkAvailable;
let midX, midY;
let instance;
let maxSphereOccurence;
let lastSelectedOption="all";
let lastSelectedYear = 2023; 


var centerY = window.innerHeight / 15;
        
        // Set the scroll position to the center
        window.scrollTo(0, centerY);

let divClickable = document.querySelectorAll('.js-sphere');
divClickable.forEach(elementDiv => {
    let sphereSelected = elementDiv.textContent.trim().toLocaleLowerCase();
    if (elementDiv){
        elementDiv.addEventListener('click',function(){
            window.location.href = '/company.html?sphere='+sphereSelected+'&company='+lastSelectedOption;
        })
    }

}

);

instance = panzoom(document.getElementById('spheres'), {
    bounds: true,
    boundsPadding: 0.3,
    transformOrigin: {x: 0.5, y: 0.5},
    minZoom: 0.3,
    maxZoom: window.innerWidth > 1200 ? 1 : 2,
});
instance.on('panstart', function(e) {
    linkTimer = setTimeout(()=> {
        linkAvailable = false;
    }, 50);
});
instance.on('panend', function(e) {
    linkAvailable = true;
});

if(window.innerWidth > 1200) {
    instance.moveTo(-(document.getElementById('spheres').clientWidth/6), -(document.getElementById('spheres').clientHeight/2.6));
    instance.smoothZoom(window.innerWidth/2, window.innerHeight/2, 0.3);
} else {
    instance.moveTo(-(document.getElementById('spheres').clientWidth/6), -(document.getElementById('spheres').clientHeight/9));
    instance.smoothZoom(window.innerWidth/2, window.innerHeight/2, 1.8);
}

midX = instance.getTransform().x
midY = instance.getTransform().y

document.querySelector('.js-zoom-in').addEventListener('click', ()=> {
    instance.smoothZoom(window.innerWidth/2, window.innerHeight/2, 1.3);
})
document.querySelector('.js-zoom-out').addEventListener('click', ()=> {
    instance.smoothZoom(window.innerWidth/2, window.innerHeight/2, 0.7);
})



// Function to fetch JSON data from a file
async function fetchJSONFile(filename) {
    try {
        const response = await fetch(filename);
        if (!response.ok) {
            throw new Error('Failed to fetch file');
        }
        return await response.json();
    } catch (error) {
        console.error(error);
    }
}


// Initialize event
window.onload = async function() {
    showMobileMessage();
    // Your method or function to run when the page loads
    // Call your method here
    const data = await fetchJSONFile(data_filename);
    let sphereList = sphereListFrequency(data);
    maxSphereOccurence = findMostFrequentSphereByCompany(data).Occurrences

    const companyData = await filterDataByCompany('all');
    const companyDateData = filterCompanyDataByDate('2023', companyData)
    fillSpheres(companyDateData)
    
};

function findMostFrequentSphereByCompany(data) {
    const companyMap = {};

    // Count occurrences of each sphere for each company
    data.forEach(entry => {
        const sphere = entry.sphere;
        const company = entry.company;

        if (!companyMap[company]) {
            companyMap[company] = {};
        }

        companyMap[company][sphere] = (companyMap[company][sphere] || 0) + 1;
    });

    // Find the company with the maximum occurrences of a sphere
    let maxOccurrences = 0;
    let mostFrequentCompany = null;
    let mostFrequentSphere = null;
    for (const company in companyMap) {
        const spheres = companyMap[company];
        for (const sphere in spheres) {
            if (spheres[sphere] > maxOccurrences) {
                maxOccurrences = spheres[sphere];
                mostFrequentCompany = company;
                mostFrequentSphere = sphere;
            }
        }
    }

    return { 
        "MostFrequentSphere": mostFrequentSphere, 
        "CompanyWithMostOccurrence": mostFrequentCompany, 
        "Occurrences": maxOccurrences 
    };
}

// Example data

const company_file = 'assets/js/companies.json'; 
const data_filename = 'assets/js/sphere-watch-data-new.json';
// Provide the path to your JSON file


// Populating options in index page.
let selectElement = document.querySelector('.js-company-options');
// Populate options
fetchJSONFile(company_file)
    .then(data => {
        data.forEach(function(company) {
            var option = document.createElement('option'); 
            option.text = company.name; // Assuming value to be lowercase, you can change it according to your requirement
            selectElement.add(option);
         });
        selectElement.value = 'all'
    })
    .catch(error => {
        console.error(error);
    });

function fillSpheres(sphereValues, companyColor){
    const spheres = document.querySelectorAll('.js-sphere');
    let maxSphereLevel;
    spheres.forEach( sphere => {
        const label = sphere.querySelector('.c-sphere__label').textContent.trim();
        let value = sphereValues[label.toLocaleLowerCase()];
        //const fillPercentage = (value / maxSphereOccurence) * 100; // Assuming maximum value is 5    
        // Apply the fill style

        maxSphereLevel = (Object.keys(sphereValues).length === 0) ? 25 : 40;
        
        // Random generator for values
        var randomNumber = Math.random();
        // Scale the random number to fit the range [10, 30]
        var randomValue = Math.floor(randomNumber * (30 - 10 + 1)) + 10;
        

        
        const fillPercentage = (randomValue / maxSphereLevel) * 100; // Assuming maximum value is 5
        const gradientColor = `radial-gradient(circle, ${companyColor} 0%, transparent 50%)`;
        sphere.querySelector('.js-sphere-fill').style.background = gradientColor;
        sphere.querySelector('.js-sphere-fill').style.width = `${fillPercentage}%`;
    });
}



///////////////////////////////
// Event Listener block
///////////////////////////////

// Get date range
let slider = document.getElementById('daterange');
slider.addEventListener('input', async function() {
    lastSelectedYear = slider.value;
    const companyData = await filterDataByCompany(lastSelectedOption);
    const companyDateData = filterCompanyDataByDate(lastSelectedYear, companyData)
    fillSpheres(companyDateData)
    // You can use the 'value' variable here for further processing
});

// Selected option
// Add event listener for change event
selectElement.addEventListener('change', async function() {
    // Get the selected option
    let currentSelectedCompanyColor;
    var selectedOption = selectElement.value;
    lastSelectedOption =  selectedOption;
    // Do something with the selected option
    const companyData = await filterDataByCompany(selectedOption)
    
    const companyDateData = filterCompanyDataByDate(lastSelectedYear, companyData)
    const companyColor = await getCompanyColor(selectedOption)
    currentSelectedCompanyColor =  (Object.keys(companyColor).length === 0) ? '#ff8164' : companyColor[0].color;


    fillSpheres(companyDateData,currentSelectedCompanyColor)
    putCurrentOptionInSpheres()
    
   
});

//////////////////////////
// Function blocks
/////////////////////////

async function getMaximumDate(){
    const data = await fetchJSONFile(data_filename);
    const dates = [...data.map(e => e.date)]
    const years = [...dates.map(d => {
    if (d) { // Check if d is defined
        return parseInt(d.split("-")[0]);
    }
    })];
    clearYears = years.filter(year => year !== undefined)
    return clearYears.sort().reverse()[0] 
}

// Put the selection option of the company in the url
function putCurrentOptionInSpheres(){
    let allHRefs = document.getElementsByTagName('a');
    for (refs in allHRefs) {

        
        if(allHRefs[refs].href && allHRefs[refs].href.includes('company')){
            // Remove the selection if there is some.
            let beforeCompanyName= allHRefs[refs].href.lastIndexOf('=');
            if(allHRefs[refs].href.substring(beforeCompanyName,allHRefs[refs].href.length) > 0){
                allHRefs[refs].href = allHRefs[refs].href.slice(0,beforeCompanyName+1)
            }
            // Now apped a new selection
            allHRefs[refs].href += lastSelectedOption;

        }
        
    }
       
    
}

async function getCompanyColor(companyName){
    const data = await fetchJSONFile(company_file);
    return data.filter(c => {
        if (c.name == companyName) {
            return c.color;
        }
    })
}


async function filterDataByCompany(selectedOption) {
    const data = await fetchJSONFile(data_filename);
    if (selectedOption  === 'all') return data;
    return data.filter( entry => entry.company === selectedOption)
}
function filterCompanyDataByDate(yearOption, data) {
    
    const frequency = data
        .filter(entry => entry.date !== undefined)
        .filter(entry => entry.date.split("-")[0] <= yearOption)
        .map(data => data.sphere)
        .reduce((acc, currentValue) => {
            acc[currentValue] = (acc[currentValue] || 0) + 1;
            return acc;
        }, {});
    return frequency;
}


function sphereListFrequency(data) {
    const frequency = data
        .map(data => data.sphere)
        .reduce((acc, currentValue) => {
            acc[currentValue] = (acc[currentValue] || 0) + 1;
            return acc;
        }, {});
    return frequency;
}


// Check if the message has already been displayed
if (localStorage.getItem("welcomeMessageDisplayed") === null) {
    // Show the welcome message modal
    var modal = document.getElementById("welcomeModal");
    var closeButton = document.getElementsByClassName("close")[0];
    
    modal.style.display = "block";

    // Close the modal when the close button is clicked
    closeButton.onclick = function() {
        modal.style.display = "none";
        // Store in localStorage that the message has been displayed
        localStorage.setItem("welcomeMessageDisplayed", "true");
    }

    // Close the modal when user clicks outside of it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
            // Store in localStorage that the message has been displayed
            localStorage.setItem("welcomeMessageDisplayed", "true");
        }
    }
}


function isMobileDevice() {
    return Math.min(window.screen.width, window.screen.height) < 414 || navigator.userAgent.indexOf("Mobi") > -1;
  }

  // Function to display the message if the user is on a mobile device
  function showMobileMessage() {
    
    if (isMobileDevice()) {  
      
      document.getElementById('mobileMessage').style.display = 'block';
     
      document.getElementById('websiteContent').style.display = 'none';
    }
    else {
        document.getElementById('mobileMessage').style.display = 'none';
     
      document.getElementById('websiteContent').style.display = 'block';
    }
  }